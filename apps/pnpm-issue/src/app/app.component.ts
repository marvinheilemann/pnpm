import { Component } from '@angular/core';

@Component({
  selector: 'ludicrous-narwhals-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'pnpm-issue';
}
